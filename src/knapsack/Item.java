/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package knapsack;

/**
 *
 * @author Drew
 */
public class Item
{
    private int val;
    private int siz;
    private String nam;
    
    public Item(String name, int value, int size)
    {
        nam = name;
        val = value;
        siz = size;
        
    }

    
    public int getValue()
    {
        return val;
        
    }
    public int getSize(){
        
        return siz;
    }
    
    @Override
    public String toString()
    {
        return nam;
        
    }
    
}
