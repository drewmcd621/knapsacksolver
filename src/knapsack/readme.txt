Drew McDermott
adm75@pitt.edu

Knapsack Problem

	The Knapsack Problem involves items of different sizes 
	being placed in a fixed size knapsack.  The solution is
	finding the highest value of items that can fit in the sack.

Methods

	This program uses three different methods to solve:

		-Recursive

		-Recursive with memo

		-Dynamic programming

	The number of calls or comparisons is logged so that the
	ammount of work required for each method can be compared.