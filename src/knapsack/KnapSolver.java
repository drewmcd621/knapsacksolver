/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package knapsack;

import java.util.List;
/**
 *
 * @author Drew
 */
public class KnapSolver 
{
    
    List<Item> itms;
    Sack sck;
    
    int sSize;
            
    public int CALLS_MEMO;
    public int CALLS_RECUR;
    public int COMPS_DYNAM;
    
    public KnapSolver(Sack Knapsack, List<Item> Items)
    {
        itms = Items;
        sck = Knapsack;
        sSize = sck.getSize();
        
        
        
    }
    
    
    
    public Sack RecurseNoMemo()
    {
        CALLS_RECUR = 0;
        Sack s = new Sack(sSize);
        return Recurse(s, itms);
    }
    
    public Sack RecurseWithMemo()
    {
        Sack[] mem = new Sack[sSize+1];
        
        for(int i = 0; i < sSize+1; i++)
        {
            //Set optimal value to -1
            mem[i] = new Sack(i);
            mem[i].setValue(-1);
        }
        CALLS_MEMO = 0;
        RecurseMemo(sSize, itms, mem);
        
        return mem[sSize];
    }
    
    public Sack Dynamic()
    {
        Sack[] mem = new Sack[sSize+1];
        
        for(int i = 0; i < sSize+1; i++)
        {
            
            mem[i] = new Sack(i);

        }
        
        COMPS_DYNAM = 0;
        
        
        for(int size = 0; size <= sSize; size++)
        {
            int mVal = 0;
            int mInd = -1;
            int oSize = -1;
            
            int nSize;
            int tVal;

            for(int itm = 0; itm < itms.size(); itm++)
            {
                COMPS_DYNAM++;
                
                nSize = size - itms.get(itm).getSize();
                
                
                if(nSize < 0) continue;
                
                tVal = mem[nSize].getValue() + itms.get(itm).getValue();
                
                if(tVal > mVal)
                {
                    mVal = tVal;
                    mInd = itm;
                    oSize = nSize;
                    
                }
                
            }
            
            if(mInd == -1) continue;
            
            mem[size].copyItems(mem[oSize]);
            mem[size].addItem(itms.get(mInd));
        }
        return mem[sSize];
        
    }
    
    private int RecurseMemo(int sSack, List<Item> Items, Sack[] Memo)
    {  
        CALLS_MEMO++;
         if(Memo[sSack].getValue() >= 0)
        {
            return Memo[sSack].getValue();
            
        }
         
             
         
        
        
        if(sSack <= 0)
        {
            Memo[sSack].setValue(0);
            return 0; //Base
        }
        
        
        
       
        
        int hVal = -1;
        int hInd = -1;
        int sInd = -1;
        
        int tVal;
        int nSize;
        
        for(int i = 0; i < Items.size(); i++)
        {
            nSize = sSack - Items.get(i).getSize();
            
            if(nSize >= 0)
            {    
                tVal = RecurseMemo(nSize, Items, Memo) + Items.get(i).getValue();
            }
            else
            {
                tVal = -1;   
            }
            
            if(tVal > hVal){
                hVal = tVal;
                hInd = i;
                sInd = nSize;
            }
            
        }
        
        if(Memo[sSack].getValue() < 0) Memo[sSack].setValue(0);
        
        if(hInd >= 0)
        {
            Memo[sSack].copyItems(Memo[sInd]);
            Memo[sSack].addItem(Items.get(hInd));
        }
        else{
            //Nothing fits
            
        }
        
        
        
        return Memo[sSack].getValue();
        
        
        
    }
    
    private Sack Recurse(Sack sSack, List<Item> Items)
    {
        CALLS_RECUR++;
         if(sSack.getSize() <= 0)
         {
            return new Sack(0); //Base
         }
        
        
        
       
        int SckSize = sSack.getSize();
        int hVal = -1;
        int hInd = -1;
        Sack mSack = new Sack(0);
        
        int tVal;
        int nSize;
        
        
        for(int i = 0; i < Items.size(); i++)
        {
            nSize = SckSize - Items.get(i).getSize();
            Sack tSack;
            
            if(nSize >= 0)
            {  
                tSack = new Sack(nSize);
                tVal = Recurse(tSack, Items).getValue() + Items.get(i).getValue();
            }
            else
            {
                tVal = -1;
                tSack = new Sack(0);
            }
            
            if(tVal > hVal)
            {
                hVal = tVal;
                hInd = i;
                mSack = tSack;
            }
            
        }
        
        sSack.copyItems(mSack);
        
        //sSack.setSize(SckSize);
        if(hInd >= 0){
            sSack.addItem(Items.get(hInd));
        }
        
        return sSack;
        
    }
    
   
    
    
    
    
    
}
