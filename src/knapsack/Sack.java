/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package knapsack;
import java.util.List;
import java.util.ArrayList;
/**
 *
 * @author Drew
 */
public class Sack {
    
    private int sze;
    private int optVal;
    private List<Item> itms;
    private int rem;
    
    public Sack(int Size)
    {
        sze = Size;
        rem = Size;
        itms = new ArrayList<Item>();
        optVal = 0;
        
    }
    
    public void setSize(int size)
    {
        int tsize = sze;
        sze = size;
        
        rem += (size - tsize);
        
    }
    
    public int getSize(){
        
        return sze;
    }
    
    public void setValue(int v)
    {
        optVal = v;
        
    }
    public int getValue()
    {
        return optVal;
    }
    
    public void addItem(Item i)
    {
        itms.add(i);
        
        rem -= i.getSize();
        optVal += i.getValue();
    }
    public int getSpace()
    {
        
        return rem;
    }
    public List<Item> getAllItems()
    {
        return itms;
    }
    
    public void copyItems(Sack s)
    {
        for(Item i : s.getAllItems())
        {
            this.addItem(i);
        }
        
    }
    
    public String toString()
    {
        String s = "";
        for(Item i : itms)
        {
            s += i.toString() + " @ " + i.getValue() +"\n";
        }
        s += "-------------------" + "\n";
        s += "Total value: " + optVal;
        return s;
        
    }
}
