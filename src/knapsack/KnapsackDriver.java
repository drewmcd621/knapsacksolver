/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package knapsack;

import java.util.ArrayList;

/**
 *
 * @author Drew
 */
public class KnapsackDriver {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) 
    {
        Sack s = new Sack(205);
        ArrayList<Item> items = new ArrayList<Item>();
        
        items.add(new Item("Platinum",1000,60));
        items.add(new Item("Gold",100,16));
        items.add(new Item("Silver",50,12));
        items.add(new Item("Pickle Jar",5,6));
        
        KnapSolver kr = new KnapSolver(s,items);
        
        System.out.println(kr.RecurseWithMemo());
        System.out.println(kr.CALLS_MEMO);
        
        System.out.println();
        
        System.out.println(kr.Dynamic());
        System.out.println(kr.COMPS_DYNAM);
        
        System.out.println();
        
        System.out.println(kr.RecurseNoMemo());
        System.out.println(kr.CALLS_RECUR);
    }
        
        
   
}
